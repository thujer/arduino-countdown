/*
   	Copyright (c) 2019, Tomas Hujer

    @author Tomas Hujer
    @version 0.1 10/04/2019 
   
   	Purpose: Countdown timer
	Call .set method to set time and check .timeout method.
	True value will be returned after time elapsed.
	If You need to monitor current timer value, its possible via .get method.

   	Permission to use, copy, modify, and/or distribute this software for
   	any purpose with or without fee is hereby granted, provided that the
   	above copyright notice and this permission notice appear in all copies.

   	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
   	WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
   	WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
   	BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
   	OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
   	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
   	ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
   	SOFTWARE.
 */

#include <Arduino.h>

#include "Countdown.h"

void Countdown::set(unsigned long ms) {
	this->countdownDelay = ms;
	this->countdownOnStart = millis();
}

 unsigned long Countdown::get() {
	 return (this->countdownDelay - (millis() - this->countdownOnStart));
 }

bool Countdown::timeout() {

	if(millis() - this->countdownOnStart >= this->countdownDelay) {
		return true;
	} else {
		return false;
	}
}
