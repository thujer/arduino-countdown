## Countdown

Universal timer for C++
===

Example of using
```
Countdown* timer = new Countdown();

void setup() {
    timer->set(1000);
}

void loop() {
    if(timer->timeout()) {
        timer->set(1000);
    }

    Serial.printf("Time remains: %s\r\n", timer->get());
}
```

----
This software is written by Tomas Hujer for various projects and is licensed under The MIT License.<br>
